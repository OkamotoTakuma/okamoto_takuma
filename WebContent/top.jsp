<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板システム</title>
    </head>
    <body>
        <div class="main-contents">
            <div class="header">
                <a href="signup">新規登録</a>
                <a href="setting">編集</a>
            </div>


    <div class="messages">
    <c:forEach items="${messages}" var="message">
            <div class="message">
                <div class="account-name">
                    <span class="login_id"><c:out value="${message.login_id}" /></span>
                    <span class="name"><c:out value="${message.name}" /></span>
                </div>
                <div class="store"><c:out value="${message.store}" /></div>
                <div class="department"><c:out value="${message.department}" /></div>
                <div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
            </div>
    </c:forEach>
</div>

            <div class="copyright"> Copyright(c)OkamotoTakuma</div>
        </div>
    </body>
</html>
