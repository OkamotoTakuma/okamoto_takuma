<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <br /> <label for="login_id">ログインID</label> <input name="login_id" id="login_id" />（半角英数字[azAZ0*9]で6文字以上20文字以下）<br />

                <label for="password">登録用パスワード</label> <input
                    name="password" type="password" id="password" />（記号を含む全ての半角文字で6文字以上20文字以下） <br />
                    <label for="password2">確認用パスワード</label> <input
                    name="password2" type="password" id="password2" /> <br /> <label
                    for="name">名称</label> <input name="name" id="name" /> （個人名10文字以内）<br />
                <label for="srtore">支店</label> <input name="store" id="store" /> <br />
                <label for="department">部署・役職</label> <input name="department" id="department" /> <br />
                <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
            </form>
            <div class="copyright">Copyright(c)OkamotoTakuma</div>
        </div>
    </body>
</html>