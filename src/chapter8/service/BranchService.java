package chapter8.service;

import static chapter8.utils.CloseableUtil.*;
import static chapter8.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter8.beans.Branch;
import chapter8.beans.UserBranchDivision;
import chapter8.dao.BranchDao;
import chapter8.dao.UserBranchDivisionDao;



public class BranchService {

    public void register(Branch branch) {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            branchDao.insert(connection, branch);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    private static final int LIMIT_NUM = 1000;

    public List<UserBranchDivision> getBranch() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserBranchDivisionDao branchDao = new UserBranchDivisionDao();
    		List<UserBranchDivision> ret = branchDao.getUserBranchDivision(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

}