package chapter8.service;

import static chapter8.utils.CloseableUtil.*;
import static chapter8.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter8.beans.User;
import chapter8.beans.UserCatalog;
import chapter8.dao.UserCatalogDao;
import chapter8.dao.UserDao;


public class CatalogService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<UserCatalog> getCatalog() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserCatalogDao messageDao = new UserCatalogDao();
    		List<UserCatalog> ret = messageDao.getUserCatalog(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

}