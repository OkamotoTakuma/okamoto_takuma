package chapter8.service;

import static chapter8.utils.CloseableUtil.*;
import static chapter8.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter8.beans.Division;
import chapter8.beans.UserBranchDivision;
import chapter8.dao.DivisionDao;
import chapter8.dao.UserBranchDivisionDao;





public class DivisionService {

    public void register(Division division) {

        Connection connection = null;
        try {
            connection = getConnection();

            DivisionDao divisionDao = new DivisionDao();
            divisionDao.insert(connection, division);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    private static final int LIMIT_NUM = 1000;

    public List<UserBranchDivision> getDivision() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserBranchDivisionDao divisionDao = new UserBranchDivisionDao();
    		List<UserBranchDivision> ret = divisionDao.getUserBranchDivision(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

}