package chapter8.dao;

import static chapter8.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter8.beans.User;
import chapter8.exception.NoRowsUpdatedRuntimeException;
import chapter8.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO user ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", store");
            sql.append(", department");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // store
            sql.append(", ?"); // department
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getStore());
            ps.setInt(5, user.getDepartment());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            sql.append(", password = ?");
            sql.append(", name = ?");
            sql.append(", store = ?");
            sql.append(", department = ?");
            sql.append(", description = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getStore());
            ps.setInt(5, user.getDepartment());
            ps.setInt(6, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}