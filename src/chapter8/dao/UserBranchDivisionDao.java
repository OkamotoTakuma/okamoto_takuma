package chapter8.dao;

import static chapter8.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter8.beans.UserBranchDivision;
import chapter8.exception.SQLRuntimeException;

public class UserBranchDivisionDao {

    public List<UserBranchDivision> getUserBranchDivision(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("user.id as id, ");
            sql.append("user.login_id as login_id, ");
            sql.append("user.name as name, ");
            sql.append("user.store as store, ");
            sql.append("user.department as department, ");
            sql.append("branch.branch_name as branch_name, ");
            sql.append("division.division_name as division_name, ");
            sql.append("user.created_date as created_date ");
            sql.append("FROM user ");
            sql.append("INNER JOIN branch ");
            sql.append("ON branch.branch_name = user.id ");
            sql.append("INNER JOIN division ");
            sql.append("ON division.division_name = user.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserBranchDivision> ret = toUserBranchDivisionList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserBranchDivision> toUserBranchDivisionList(ResultSet rs)
            throws SQLException {

        List<UserBranchDivision> ret = new ArrayList<UserBranchDivision>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
            	String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                int store = rs.getInt("store");
                int department = rs.getInt("department");
                String branch_name = rs.getString("branch_name");
                String division_name = rs.getString("division_name");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserBranchDivision message = new UserBranchDivision();
                message.setId(id);
                message.setLogin_id(login_id);
                message.setName(name);
                message.setStore(store);
                message.setDepartment(department);
                message.setBranch_name(branch_name);
                message.setDivision_name(division_name);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}