package chapter8.dao;

import static chapter8.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter8.beans.Branch;
import chapter8.exception.SQLRuntimeException;

public class BranchDao {

    public void insert(Connection connection, Branch branch) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO branch ( ");
            sql.append("branch_name");
            sql.append(") VALUES (");
            sql.append("?"); // branch_name
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, branch.getBranch_name());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Branch> toBranchList(ResultSet rs) throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String branch_name = rs.getString("branch_name");


                Branch branch = new Branch();
                branch.setId(id);
                branch.setBranch_name(branch_name);
                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public Branch getBranch(Connection connection, int id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM branch WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		ResultSet rs = ps.executeQuery();
    		List<Branch> branchList = toBranchList(rs);
    		if (branchList.isEmpty() == true) {
    			return null;
    		} else if (2 <= branchList.size()) {
    			throw new IllegalStateException("2 <= userList.size()");
    		} else {
    			return branchList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

}