package chapter8.dao;

import static chapter8.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter8.beans.Division;
import chapter8.exception.SQLRuntimeException;

public class DivisionDao {

    public void insert(Connection connection, Division division) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO division ( ");
            sql.append("division_name");
            sql.append(") VALUES (");
            sql.append("?"); // division_name
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, division.getDivision_name());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Division> toDivisionList(ResultSet rs) throws SQLException {

        List<Division> ret = new ArrayList<Division>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String division_name = rs.getString("division_name");


                Division division = new Division();
                division.setId(id);
                division.setDivision_name(division_name);
                ret.add(division);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public Division getDIvision(Connection connection, int id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM division WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		ResultSet rs = ps.executeQuery();
    		List<Division> divisionList = toDivisionList(rs);
    		if (divisionList.isEmpty() == true) {
    			return null;
    		} else if (2 <= divisionList.size()) {
    			throw new IllegalStateException("2 <= userList.size()");
    		} else {
    			return divisionList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }
}
