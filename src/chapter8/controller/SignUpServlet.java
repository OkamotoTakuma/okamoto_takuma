package chapter8.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter8.beans.User;
import chapter8.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setPassword(request.getParameter("password2"));
            user.setName(request.getParameter("name"));
            user.setStore(Integer.parseInt(request.getParameter("store")));
            user.setDepartment(Integer.parseInt(request.getParameter("department")));

            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        String store = request.getParameter("store");
        String department = request.getParameter("department");

        if(!login_id.matches("^[a-zA-z0-9]{6,20}$")) {
            messages.add("ログインIDは半角英数字6文字以上20文字以下です。");
        }
        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }

        if(!password.matches("^[a-zA-z0-9]{6,20}$")) {
            messages.add("パスワードは半角文字6文字以上20文字以下です。");
        } else if(!password2.equals(password)) {
        	messages.add("パスワードが不一致です。");
        }


        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください。");
        }

        if(name.length()>10) {
            messages.add("ユーザー名は10文字以内で入力してください。");
        }
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名称を入力してください。");
        }

        if (StringUtils.isEmpty(store) == true) {
            messages.add("支店を入力してください。");
        }

        if (StringUtils.isEmpty(department) == true) {
        	messages.add("部署・役職を入力してください。");
        }

        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}