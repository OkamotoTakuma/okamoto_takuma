package chapter8.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter8.beans.User;
import chapter8.service.CatalogService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("signupUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        List<UserCatalog> catalog = new CatalogService().getCatalog();

        request.setAttribute("catalog", catalog);
        request.setAttribute("isShowMessageForm", isShowMessageForm);


        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}