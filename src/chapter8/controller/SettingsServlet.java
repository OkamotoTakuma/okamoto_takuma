package chapter8.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter8.beans.User;
import chapter8.exception.NoRowsUpdatedRuntimeException;
import chapter8.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        //セッションよりログインユーザーの情報を取得
        User loginUser = (User) session.getAttribute("loginUser");
        //ログインユーザー情報のidを元にDBからユーザー情報取得
        User editUser = new UserService().getUser(loginUser.getId());
        request.setAttribute("editUser", editUser);

        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			session.setAttribute("loginUser", editUser);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLogin_id(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setStore(Integer.parseInt(request.getParameter("store")));
		editUser.setDepartment(Integer.parseInt(request.getParameter("department")));
		return editUser;
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
	    String store = request.getParameter("store");
	    String department = request.getParameter("department");

	    if(!login_id.matches("^[a-zA-z0-9]{6,20}$")) {
            messages.add("ログインIDは半角英数字6文字以上20文字以下です。");
        }
        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ログインIDを入力してください");

        if(!password.matches("^[-~｡-ﾟ]{6,20}$")) {
                messages.add("パスワードは半角文字6文字以上20文字以下です。");
        }
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		} else {
        	messages.add("パスワードが不一致です。");
        }
		if(name.length()>10) {
            messages.add("ユーザー名は10文字以内で入力してください。");
        }
		if (StringUtils.isEmpty(name) == true) {
	            messages.add("名称を入力してください。");
	    }
		if (StringUtils.isEmpty(store) == true) {
	            messages.add("支店を入力してください。");
	    }
	    if (StringUtils.isEmpty(department) == true) {
	        	messages.add("部署・役職を入力してください。");
	        }
	     }

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}