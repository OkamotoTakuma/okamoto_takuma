package chapter8.beans;

import java.io.Serializable;

public class Division implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String division_name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDivision_name() {
		return division_name;
	}
	public void setDivision_name(String division_name) {
		this.division_name = division_name;
	}

}